<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model','getData');
	}
	
	public function index(){
		$this->load->view('success');	
	}
	public function user(){
		$data = array();
		$data['users'] = $this->getData->get_user();
		$this->load->view('user_infomation',$data);	
	}
}
?>
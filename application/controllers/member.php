<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user_model','getData');
		$this->load->helper('url');
	}
	
	public function login(){
		$this->load->view('login');	
	}
	
	public function check_login(){
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		/*$new = array(
			'user_name' =>'admin',
			'password' =>md5('admin123'),
			'email'  	=>'adminvip@gmail.com'
		);
		$this->db->insert('user_info',$new);
		die;*/
		$check = $this->getData->check($email,$pass);
		if($check){
			$this->session->set_userdata('id',$check['id']);
			$this->session->set_userdata('email',$check['email']);
			header("location: http://adminvip.com");	
		}else{
			header('location:login');	
			
		}
	}
	public function logout(){
		isset($_SESSION['id']) ? session_unset($_SESSION['id']) : '';
		isset($_SESSION['email']) ? session_unset($_SESSION['email']) : '';
		header('location:login');	
	}
	
}
?>
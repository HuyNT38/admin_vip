<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller'] = "home/index";
$route['app']				= "home/index";
$route['404_override'] = '';

$route['user']				= "home/user";


$route['login'] = 'member/login';

$route['login-process'] = 'member/check_login';
$route['logout']        = 'member/logout';



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	
	public function check($email,$password){
		$pass = md5($password);
		$where = array('email'=>$email,'password'=>$pass);
		$check = $this->db->where($where)->get('users_info')->row_array();
		return $check;	
	}
	
	public function get_user(){
			$data = $this->db->get('users')->result_array();
			foreach ($data as $k => $d) {
			$data[$k]['expire'] = date('d/m/Y',$d['expire']);
			}
			return $data; 	
	}
}
?>
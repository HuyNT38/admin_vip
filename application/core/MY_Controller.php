<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $template  =  'template/app';
	public $user_id;
	public $user_email;
	public $user_level;
	public $user_token;
	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','getData');
		$this->check_session();
 	}
	
	public function check_session()
	{
		if(!isset($_SESSION['id']) || !isset($_SESSION['email'])){
				header("location: login");
		}
		$this->user_id    = $_SESSION['id'];
		$this->user_email = $_SESSION['email'];
		
	}
		
}
?>